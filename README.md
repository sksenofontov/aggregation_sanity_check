Script of views, clicks and checkouts consistency checking.

To run the script type:

```
npm install
npm run start -- [arguments]
```

To get some information about required arguments, type:

```
npm run start -- --help
```