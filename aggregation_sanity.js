const _ = require('lodash');
const Promise = require('bluebird');
const program = require('commander');
const ClickHouse = require('@apla/clickhouse');
const { ObjectId, MongoClient } = require('mongodb');
const { URL } = require('url');
const moment = require('moment');

const erlangLogsDbMarker = /^kairion_reporting_\d+$/;
const dailyReportsDbMarker = /delivery_reports$/;
const sources = {};

program
  .option('--shops-uri <string>', 'Mongo URI to connect to', uriValidate)
  .option('--delivery-admin-uri <string>', 'Mongo URI to connect to', uriValidate)
  .option('--delivery-user-uri <string>', 'Mongo URI to connect to', uriValidate)
  .option('--erlang-admin-uri <string>', 'Mongo URI to connect to', uriValidate)
  .option('--erlang-user-uri <string>', 'Mongo URI to connect to', uriValidate)
  .option('-s, --start <string>', 'Start moment of logs YYYY-MM-DD', dateStringValidate)
  .option('-e, --end <string>', 'End moment of logs YYYY-MM-DD', dateStringValidate)
  .option('-c, --clickhouse-host <string>', 'Clickhouse host to connect to', stringValidate)
  .option('-p, --clickhouse-port <number>', 'Clickhouse port to connect to', numberValidate(true))
  .option('--clickhouse-user <string>', 'Clickhouse user', stringValidate)
  .option('--clickhouse-password <string>', 'Clickhouse password', stringValidate)
  .option('--shop-drupal-id <number>', 'Optional. Shop drupal id to request', numberValidate(false))
  .option('--margin <number>', 'Default = 0. Relative margin in reports data', numberValidate(false))
  .option('--absolute-deviation <number>', 'Default = 10. Absoule deviation in reports data', numberValidate(false))
  .option('--show-positive-results <boolean>', 'Show even equal values of compare', booleanValidate(false))
  .parse(process.argv);

uriExistingCheck(program);
timeBordersExistingCheck(program);

const momentEnd = moment.utc(program.end, 'YYYY-MM-DD');
const tomorrowAfterEndDate = momentEnd.date(momentEnd.date() + 1);

const campaignIdsToDrupalIds = {};
const drupalIdsToCampaignIds = {};
const shopDrupalIdToShopId = {};
const shopIdToShopDrupalId = {};

const margin = program.margin || 0;
const absoluteDeviation = program.absoluteDeviation || 10;
(async () => {
  await connectToSources(program);
  await fulfilClickhouseCampaignDictinaries(sources.clickhouseClient);
  await fulfilShopsDictinary();

  const {
    views: clickhouseViews,
    checkouts: {
      checkouts: clickhouseCheckouts,
      sales: clickhouseSales,
      turnover: clickhouseTurnover
    }
  } = await getClickhouseReports(sources.clickhouseClient, program);
  console.log('clickhouseViews', clickhouseViews);

  const {
    views: erlangViews,
    checkouts: erlangCheckouts,
    turnover: erlangTurnover,
    sales: erlangSales
  } = await getErlangData(program);
  console.log('erlangViews', erlangViews);

  const {
    views: deliveryReportsViews,
    checkouts: deliveryReportsCheckouts,
    turnover: deliveryReportsTurnover,
    sales: deliveryReportsSales
  } = await getDeliveryReportsData(program);
  console.log('deliveryReportsViews', deliveryReportsViews);

  console.log('\n--- Let\'s comare erlang data with Delivery daily reports collection ---');
  compare(erlangViews, deliveryReportsViews, 'Erlang views', 'CCC reports views');
  compare(erlangCheckouts, deliveryReportsCheckouts, 'Erlang checkouts', 'CCC reports checkouts');
  compare(erlangTurnover, deliveryReportsTurnover, 'Erlang turnover', 'CCC reports turnover');
  compare(erlangSales, deliveryReportsSales, 'Erlang sales', 'CCC reports sales');

  console.log('\n--- Let\'s comare Delivery daily reports data with Clickhouse ---');
  compare(deliveryReportsViews, clickhouseViews, 'CCC reports views', 'Clickhouse views');
  compare(deliveryReportsCheckouts, clickhouseCheckouts, 'CCC reports checkouts', 'Clickhouse checkouts');
  compare(deliveryReportsTurnover, clickhouseTurnover, 'CCC reports turnover', 'Clickhouse turnover');
  compare(deliveryReportsSales, clickhouseSales, 'CCC reports sales', 'Clickhouse sales');

  console.log('\n--- Let\'s comare erlang data with Clickhouse ---');
  compare(erlangViews, clickhouseViews, 'Erlang views', 'Clickhouse views');
  compare(erlangCheckouts, clickhouseCheckouts, 'Erlang checkouts', 'Clickhouse checkouts');
  compare(erlangTurnover, clickhouseTurnover, 'Erlang turnover', 'Clickhouse turnover');
  compare(erlangSales, clickhouseSales, 'Erlang sales', 'Clickhouse sales');

  await closeConnections();
})().catch((e) => reportError(`An error occured: ${e}, stack: ${e.stack}`));

function compare (first = {}, second = {}, firstSourceName = '', secondSourceName = '') {
  const result = {
    nonExistingKeys: {
      inFirstElem: [],
      inSecondElem: []
    },
    differentValues: {}
  };

  for (const shopId in first) {
    const secondShopData = second[shopId];
    if (typeof secondShopData === 'undefined') {
      result.nonExistingKeys.inSecondElem.push(shopId);
      console.error(`Finding nonexisting shopId in ${secondSourceName}: ${shopId}`);
      continue;
    }

    const firstShopData = first[shopId];
    for (const campaignId in firstShopData) {
      const secondShopCampaignData = second[shopId][campaignId];
      if (typeof secondShopCampaignData === 'undefined') {
        result.nonExistingKeys.inSecondElem.push(`${shopId}:${campaignId}`);
        console.error(`Finding nonexisting campaignId in ${secondSourceName}: ${shopId}->${campaignId}`);
        continue;
      }

      const firstShopCampaignData = first[shopId][campaignId];
      if (firstShopCampaignData !== secondShopCampaignData) {
        const diff = Math.abs(firstShopCampaignData - secondShopCampaignData);
        result.differentValues[`${shopId}:${campaignId}`] = diff;
        if (
          (diff > absoluteDeviation) &&
          (!margin || ((diff / firstShopCampaignData) * 100 >= margin))
        ) {
          console.error(`There is a difference between ${firstSourceName} (value: ${firstShopCampaignData}) and ${secondSourceName} (value: ${secondShopCampaignData}) for ${shopId}->${campaignId}: ${diff} abs, ${Math.round((diff / firstShopCampaignData) * 100)}%`);
        }
      } else if (program.showPositiveResults) {
        console.log(`${firstSourceName} (value: ${firstShopCampaignData});${secondSourceName} (value: ${secondShopCampaignData}) for ${shopId}->${campaignId}`);
      }
    }
  }

  for (const shopId in second) {
    const firstShopData = first[shopId];
    if (typeof firstShopData === 'undefined') {
      result.nonExistingKeys.inSecondElem.push(shopId);
      console.error(`Finding nonexisting shopId in ${firstSourceName}: ${shopId}`);
      continue;
    }

    const secondShopData = second[shopId];
    for (const campaignId in secondShopData) {
      const firstShopCampaignData = first[shopId][campaignId];
      if (typeof firstShopCampaignData === 'undefined') {
        result.nonExistingKeys.inSecondElem.push(`${shopId}:${campaignId}`);
        console.error(`Finding nonexisting campaignId in ${firstSourceName}: ${shopId}->${campaignId}`);
      }
    }
  }

  return result;
}

async function getDeliveryReportsData ({ start, end }) {
  const { databases: dbNameList } = await sources.deliveryReports.admin.db().admin().listDatabases({ onlyNames: true });
  const { name: dbName } = dbNameList.find(({ name }) => dailyReportsDbMarker.test(name)); // only one db needed

  const shopFilterRule = program.shopDrupalId
    ? { shop_id: ObjectId(shopDrupalIdToShopId[program.shopDrupalId]) } : {};
  return sources.deliveryReports.user.db(dbName).collection('delivery_daily_reports').aggregate([{
    $match: {
      $and: [{
        date: {
          $gte: moment.utc(start, 'YYYY-MM-DD').toDate(),
          $lt: tomorrowAfterEndDate.toDate()
        }
      },
      shopFilterRule
      ]
    }
  }, {
    $group: {
      _id: { campaignId: '$campaign_id', shopId: '$shop_id' },
      views: { $sum: '$line.ais' },
      checkouts: { $sum: '$line.checkouts_pv' },
      sales: { $sum: '$line.sales_pv' },
      turnover: { $sum: '$line.revenue_pv' }
    }
  }])
    .toArray()
    .then((arr) => {
      return arr.reduce((acc, { _id: { campaignId, shopId }, views, checkouts, sales, turnover }) => {
        _.set(acc, ['views', shopId, campaignId], views);
        _.set(acc, ['checkouts', shopId, campaignId], checkouts);
        _.set(acc, ['sales', shopId, campaignId], sales);
        _.set(acc, ['turnover', shopId, campaignId], turnover);
        return acc;
      }, {});
    });
}

async function getErlangData ({ start, end }) {
  const { databases: dbNameList } = await sources.kairionReporting.admin.db().admin().listDatabases({ onlyNames: true });
  // there are many databases here
  const dbNames = dbNameList
    .filter(({ name }) => erlangLogsDbMarker.test(name)) // only one db needed
    .map(({ name }) => name);

  const dataAcc = {};
  await Promise.mapSeries(dbNames, async (name) => {
    const shopDrupalId = name.match(/(?!_)\d+$/);
    if (program.shopDrupalId && +shopDrupalId !== program.shopDrupalId) {
      return Promise.resolve();
    }
    const shopId = shopDrupalIdToShopId[shopDrupalId];
    if (!shopId) {
      console.error(`There is no shopId for a shopDrupalId: ${shopDrupalId} in erlang logs`);
      return Promise.resolve();
    }

    const adjustedUri = program.erlangUserUri.replace(/\/[\w_]+$/, `/${name}`);
    const connect = await MongoClient(adjustedUri, {
      connectTimeoutMS: 20 * 60 * 1000, // 20mins
      useNewUrlParser: true,
      useUnifiedTopology: true
    }).connect().catch((e) => reportError(`Something happend during connect to ${adjustedUri}: ${e}, stack: ${e.stack}`));

    const viewsArr = await connect.db().collection('logs').aggregate([{
      $match: {
        t: 'adview',
        cd: {
          $gte: moment.utc(start, 'YYYY-MM-DD').toDate(),
          $lt: tomorrowAfterEndDate.toDate()
        }
      }
    }, {
      $project: {
        temp: {
          $objectToArray: '$$ROOT.d'
        }
      }
    }, {
      $unwind: '$temp'
    }, {
      $group: {
        _id: '$temp.k',
        count: { $sum: 1 }
      }
    }]).toArray();

    viewsArr.forEach(({ _id: drupalId, count }) => {
      const campaignId = drupalIdsToCampaignIds[drupalId];
      if (!campaignId) {
        return console.error(`There is no campaignId for a drupalId: ${drupalId} in erlang logs`);
      }

      _.set(dataAcc, ['views', shopId, campaignId], count);
    });

    const checkoutData = await connect.db().collection('logs').aggregate([{
      $match: {
        t: 'checkout',
        cd: {
          $gte: moment.utc(start, 'YYYY-MM-DD').toDate(),
          $lt: tomorrowAfterEndDate.toDate()
        },
        a: { $exists: true }
      }
    }, {
      $project: {
        tempCampaigns: {
          $objectToArray: '$$ROOT.a'
        },
        tempProducts: {
          $objectToArray: '$$ROOT.cp.gs'
        }
      }
    }, {
      $project: {
        'cartData': {
          $reduce: {
            input: '$tempProducts',
            initialValue: { turnover: 0, sales: 0 },
            in: {
              turnover: { $add: [ { $multiply: ['$$this.v.p', '$$this.v.a'] }, '$$value.turnover'] },
              sales: { $add: [ '$$this.v.a', '$$value.sales' ] }
            }
          }
        },
        tempCampaigns: 1
      }
    }, {
      $unwind: '$tempCampaigns'
    }, {
      $group: {
        _id: '$tempCampaigns.k',
        checkouts: { $sum: 1 },
        turnover: { $sum: '$cartData.turnover' },
        sales: { $sum: '$cartData.sales' }
      }
    }])
      .toArray();

    checkoutData.forEach(({ _id: drupalId, checkouts = 0, turnover = 0, sales = 0 }) => {
      const campaignId = drupalIdsToCampaignIds[drupalId];
      if (!campaignId) {
        return console.error(`There is no campaignId for a drupalId: ${drupalId} in erlang logs`);
      }

      _.set(dataAcc, ['checkouts', shopId, campaignId], checkouts);
      _.set(dataAcc, ['turnover', shopId, campaignId], turnover);
      _.set(dataAcc, ['sales', shopId, campaignId], sales);
    });

    connect.close();
  });

  return dataAcc;
}

function getClickhouseReports (client, { start, end }) {
  return Promise.props({
    views: getClickhouseViews.apply(this, arguments),
    checkouts: getClickhouseCheckouts.apply(this, arguments)
  });
}

function getClickhouseCheckouts (client, { start, end }) {
  const stream = client.query(`
SELECT
  qsh,
  campaign_id,
  SUM(qopr) as turnover,
  SUM(arraySum(qcp.amount)) as sales,
  COUNT(eventDate) AS checkouts
FROM default.events_erlang
ARRAY JOIN pErlangCampaigns as campaign_id
PREWHERE
  qtype='co'
  ${program.shopDrupalId ? `AND qsh='${shopDrupalIdToShopId[program.shopDrupalId]}'` : ''}
WHERE
  dt BETWEEN toDateTime('${moment.utc(start, 'YYYY-MM-DD').toISOString().slice(0, -5)}', 'UTC') AND toDateTime('${tomorrowAfterEndDate.toISOString().slice(0, -5)}', 'UTC')
  // eventDate BETWEEN '${start}' AND '${end}'
  AND penv = 'live' AND qsh != ''
GROUP BY qsh, campaign_id
  `);

  // moment.utc(start, 'YYYY-MM-DD').toDate(),
  //   $lt: tomorrowAfterEndDate.toDate()

  stream.on('metadata', function (columns) {
    // do something with column list
  });

  const res = {};
  return new Promise((accept, reject) => {
    stream.on('data', function ([shopId, campaignDrupalId, turnover, sales, checkouts]) {
      const campaignId = drupalIdsToCampaignIds[campaignDrupalId];

      if (!campaignId) {
        return console.error(`There is no campaignId for a drupalId: ${campaignDrupalId} in clickhouse`);
      }

      _.set(res, ['turnover', shopId, campaignId], +turnover);
      _.set(res, ['sales', shopId, campaignId], +sales);
      _.set(res, ['checkouts', shopId, campaignId], +checkouts);
    });

    stream.on('error', reject);
    stream.on('end', () => accept(res));
  });
}

function getClickhouseViews (client, { start, end }) {
  const stream = client.query(`
SELECT
  qsh,
  qca,
  COUNT(eventDate) AS views
FROM default.events_erlang
PREWHERE
  qtype='av'
  ${program.shopDrupalId ? `AND qsh='${shopDrupalIdToShopId[program.shopDrupalId]}'` : ''}
WHERE
  eventDate BETWEEN '${start}' AND '${end}'
  AND penv = 'live' AND qsh != ''
GROUP BY qsh,qca
`);

  stream.on('metadata', function (columns) {
    // do something with column list
  });

  const res = {};

  return new Promise((accept, reject) => {
    stream.on('data', function ([shopId, campaignDrupalId, views]) {
      const campaignId = drupalIdsToCampaignIds[campaignDrupalId];
      if (!campaignId) {
        return console.error(`There is no campaignId for a drupalId: ${campaignDrupalId} in clickhouse`);
      }

      _.set(res, [shopId, campaignId], +views);
    });

    stream.on('error', reject);
    stream.on('end', () => accept(res));
  });
}

function fulfilClickhouseCampaignDictinaries (client) {
  const stream = client.query(`
    SELECT campaign_id,
        toString(drupal_id) AS drupal_id,
        shop_id
    FROM dictionaries.campaigns
  `);

  stream.on('metadata', function (columns) {
    // do something with column list
  });

  return new Promise((accept, reject) => {
    stream.on('data', function ([campaignId, drupalId, shopId]) {
      campaignIdsToDrupalIds[campaignId] = drupalId;
      drupalIdsToCampaignIds[drupalId] = campaignId;
    });

    stream.on('error', reject);
    stream.on('end', accept);
  });
}

async function fulfilShopsDictinary () {
  const shopArray = await sources.shops.user.db().collection('shops').find({}, { 'data.drupal_id': 1 }).toArray();
  shopArray.forEach(({ _id, data: { drupal_id: drupalId } }) => {
    shopDrupalIdToShopId[drupalId] = _id.toString();
    shopIdToShopDrupalId[_id.toString()] = drupalId;
  });

  if (program.shopDrupalId && !shopDrupalIdToShopId[program.shopDrupalId]) {
    await reportError(`Didn't find shop with drupal id: ${program.shopDrupalId}`);
  }
}

async function connectToSources (program) {
  const {
    clickhouseHost,
    clickhousePort,
    clickhouseUser,
    clickhousePassword,

    shopsUri,
    deliveryAdminUri,
    deliveryUserUri,
    erlangAdminUri,
    erlangUserUri
  } = program;

  sources.clickhouseClient = new ClickHouse({
    host: clickhouseHost,
    port: clickhousePort,
    auth: `${clickhouseUser}:${clickhousePassword}`,
    syncParser: true
  });

  const mongoConnections = await Promise.props({
    shops: {
      user: await MongoClient(shopsUri, {
        connectTimeoutMS: 20 * 60 * 1000, // 20mins
        useNewUrlParser: true,
        useUnifiedTopology: true
      }).connect().catch((e) => reportError(`Something happend during connect to ${shopsUri}: ${e}, stack: ${e.stack}`))
    },
    deliveryReports: {
      admin: await MongoClient(deliveryAdminUri, {
        connectTimeoutMS: 20 * 60 * 1000, // 20mins
        useNewUrlParser: true,
        useUnifiedTopology: true
      }).connect().catch((e) => reportError(`Something happend during connect to ${deliveryAdminUri}: ${e}, stack: ${e.stack}`)),
      user: await MongoClient(deliveryUserUri, {
        connectTimeoutMS: 20 * 60 * 1000, // 20mins
        useNewUrlParser: true,
        useUnifiedTopology: true
      }).connect().catch((e) => reportError(`Something happend during connect to ${deliveryUserUri}: ${e}, stack: ${e.stack}`))
    },
    kairionReporting: {
      admin: await MongoClient(erlangAdminUri, {
        connectTimeoutMS: 20 * 60 * 1000, // 20mins
        useNewUrlParser: true,
        useUnifiedTopology: true
      }).connect().catch((e) => reportError(`Something happend during connect to ${erlangAdminUri}: ${e}, stack: ${e.stack}`)),
      user: await MongoClient(erlangUserUri, {
        connectTimeoutMS: 20 * 60 * 1000, // 20mins
        useNewUrlParser: true,
        useUnifiedTopology: true
      }).connect().catch((e) => reportError(`Something happend during connect to ${erlangUserUri}: ${e}, stack: ${e.stack}`))
    }
  });

  return Object.assign(sources, mongoConnections);
}

async function closeConnections () {
  return Promise.map(Object.keys(sources), (source) => {
    if (!source) {
      return Promise.resolve();
    }

    return Promise.map(Object.keys(sources[source]), (connect) => {
      return (sources[source][connect].close && sources[source][connect].close()) || Promise.resolve();
    });
  });
}

function uriValidate (str) {
  let isUrl = true;
  try {
    new URL(str); // eslint-disable-line
  } catch (e) {
    isUrl = false;
  }

  if (!str || !isUrl) {
    return reportError(`It's not a valid uri: ${str}`, false);
  }

  return str;
}

function numberValidate (required, number) {
  return function (number) {
    if (required && isNaN(number)) {
      return reportError(`Valid number value shoud be provided: ${number}`, false);
    }

    return +number;
  };
}

function booleanValidate (required, value) {
  return function (value) {
    if (required && !['true', 'false'].includes(value)) {
      return reportError(`Valid boolean value shoud be provided: ${value}`, false);
    }

    return value === 'true';
  };
}

function stringValidate (str) {
  if (!str) {
    return reportError(`Valid string should be provided: ${str}`, false);
  }

  return str;
}

function dateStringValidate (dateString) {
  if (!moment(dateString, 'YYYY-MM-DD').isValid()) {
    return reportError(`Valid date string should be provided: ${dateString}`, false);
  }

  return dateString;
}

function timeBordersExistingCheck ({ start, end }) {
  if (start > end) {
    return reportError('Start date should be earlier then end date', false);
  }

  if (!start) {
    return reportError('Start date should be provided', false);
  }

  if (!end) {
    return reportError('End date should be provided', false);
  }
}

function uriExistingCheck ({
  clickhouseHost,
  clickhousePort,
  clickhouseUser,
  clickhousePassword,
  shopsUri,
  deliveryAdminUri,
  deliveryUserUri,
  erlangAdminUri,
  erlangUserUri
}) {
  if (!shopsUri) {
    return reportError('Please, provide shops uri to connect to', false);
  }

  if (!deliveryAdminUri) {
    return reportError('Please, provide delivery reports admin uri to connect to', false);
  }

  if (!deliveryUserUri) {
    return reportError('Please, provide delivery reports user uri to connect to', false);
  }

  if (!erlangUserUri) {
    return reportError('Please, provide erlang logs user uri to connect to', false);
  }

  if (!erlangAdminUri) {
    return reportError('Please, provide erlang logs admin uri to connect to', false);
  }

  if (!clickhouseHost) {
    return reportError('Please, provide clickhouse host to connect to', false);
  }

  if (!clickhousePort) {
    return reportError('Please, provide clickhouse port to connect to', false);
  }

  if (!clickhouseUser) {
    return reportError('Please, provide clickhouse user', false);
  }

  if (!clickhousePassword) {
    return reportError('Please, provide a password for clickhouse user', false);
  }
}

async function reportError (message, needCloseConnections = true) {
  console.error(message);
  needCloseConnections && await closeConnections();
  process.exit(1);
}
